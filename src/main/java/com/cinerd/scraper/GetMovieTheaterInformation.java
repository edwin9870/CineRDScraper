package com.cinerd.scraper;

import com.cinerd.scraper.entity.Movie;
import com.cinerd.scraper.entity.Room;
import com.cinerd.scraper.entity.Theater;
import com.cinerd.scraper.util.ISODateAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.MutableDateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Edwin Ramirez Ventur on 8/10/2017.
 */
public class GetMovieTheaterInformation {

    public static final String SPANISH_LANGUAGE = "Spanish";
    public static final String ENGLISH_LANGUAGE = "English";

    public static void main(String[] args) throws IOException {
        final Document movieHtml = Jsoup.connect("http://cinemas.com.do/cartelera/component/movies/?task=moviedetails&mid=1519").userAgent("Mozilla").timeout(3000).get();

        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new ISODateAdapter()).create();
        final Movie movie = new Movie();
        movie.setTheaters(getTheaters(movieHtml, false));
        System.out.println("json: "+gson.toJson(movie));

    }

    public static List<Theater> getTheaters(Document movieHtml, boolean allMovieHours) {
        List<Theater> theaters = new ArrayList<>();
        final int movieTheaterSize = movieHtml.select("#timings_left > h2").size();

        System.out.println("size: " + movieTheaterSize);
        for (int i = 0; i < movieTheaterSize; i++) {
            String theaterName = movieHtml.select("#timings_left > h2").get(i).text();
            Theater theater = new Theater();
            System.out.println("Theater name: " + theaterName);
            if(theaterName.contains("At the Acropolis")) {
                theaterName = "Acropolis";
            }
            theater.setName(theaterName);
            theater.setRoom(new ArrayList<>());
            final Elements theaterDetail = movieHtml.select("#timings_left div").get(i).select("span");
            String roomNumber = "";
            String language = "";
            String subLanguage = "";
            Room room = null;
            String movieFormat = "";
            for (int iTheaterDetail = 0; iTheaterDetail < theaterDetail.size(); iTheaterDetail++) {
                if (iTheaterDetail == 0) {
                    String theaterHeader = theaterDetail.get(iTheaterDetail).text();
                    theaterHeader = theaterHeader.substring(theaterHeader.indexOf(":"));
                    movieFormat = getFormat(theaterHeader);
                    roomNumber = getRoomNumber(theaterDetail.get(iTheaterDetail).text());
                    language = getLanguage(theaterDetail.get(iTheaterDetail).text());
                    subLanguage = getSubLanguage(theaterDetail.get(iTheaterDetail).text());

                    continue;
                }

                final String dayCode = theaterDetail.get(iTheaterDetail).select("strong").text();
                if (dayCode == null || dayCode.trim().length() < 1) {
                    continue;
                }

                int calendarDay = 0;
                calendarDay = getDayOfTheWeek(dayCode, calendarDay);
                final MutableDateTime dateTime = new DateTime(new Date()).toMutableDateTime();
                dateTime.dayOfWeek().set(calendarDay);
                System.out.println("calendar day: " + calendarDay + ", dayCode: " + dayCode + ", date: " + dateTime.toDate());
                final List<Date> availablesTime = getAvailableHour(dateTime.toDateTime(), theaterDetail.get(iTheaterDetail).text(), allMovieHours);
                for (Date availableTime : availablesTime) {
                    room = new Room();
                    room.setmLanguage(language);
                    room.setmSubtitle(subLanguage);
                    room.setName(roomNumber);
                    room.setmDate(availableTime);
                    room.setmFormat(movieFormat);
                    theater.getmRoom().add(room);
                }

                System.out.println("Room name: " + roomNumber);
                System.out.println("Language: " + language);
                System.out.println("subLanguage: " + subLanguage);
                System.out.println("movieFormat: " + movieFormat);


            }

            if (theater.getmRoom() == null || theater.getmRoom().isEmpty()) {
                continue;
            }
            theaters.add(theater);

        }
        return theaters;
    }

    private static int getDayOfTheWeek(String dayCode, int calendarDay) {
        switch (dayCode.toUpperCase()) {
            case "SA":
                calendarDay = DateTimeConstants.SATURDAY;
                break;
            case "DO":
                calendarDay = DateTimeConstants.SUNDAY;
                break;
            case "LU":
                calendarDay = DateTimeConstants.MONDAY;
                break;
            case "MA":
                calendarDay = DateTimeConstants.TUESDAY;
                break;
            case "MI":
                calendarDay = DateTimeConstants.WEDNESDAY;
                break;
            case "JU":
                calendarDay = DateTimeConstants.THURSDAY;
                break;
            case "VI":
                calendarDay = DateTimeConstants.FRIDAY;
                break;
        }
        return calendarDay;
    }

    /**
     * @param theaterHeader: 'Sala : 07 4DX 3D ', 'Sala : 05 3D esp.'
     * @return '4DX'
     */
    private static String getFormat(String theaterHeader) {
        Pattern p = Pattern.compile("(\\d[a-zA-Z]+|[a-zA-Z]{3})+");
        Matcher m = p.matcher(theaterHeader);
        m.find();
        final String noFormatValue = "N/A";
        try {
            String format = m.group(1);

            if(format == null || format.length() < 1) {
                format = noFormatValue;
            }else if(format.contains("esp")) {
                format = noFormatValue;
            }
            return format;
        } catch (IllegalStateException ignored) {
        }

        return noFormatValue;
    }

    /**
     * @param theaterHeader: Sa 3:50,6:30,9:30
     * @return
     */
    private static List<Date> getAvailableHour(DateTime baseDate, String theaterHeader, boolean allMovieHours) {
        List<Date> dates = new ArrayList<>();
        Pattern p = Pattern.compile("([\\d\\:]+)");
        Matcher m = p.matcher(theaterHeader);
        List<LocalDate> datesProcced = new ArrayList<>();
        while (m.find()) {
            try {

                final String dateTimeString = m.group(1);//9:30
                final String hour = dateTimeString.substring(0, dateTimeString.indexOf(":"));
                final String minutesString = dateTimeString.substring(dateTimeString.indexOf(":") + 1);

                final Date dateWithMovieSchedule = baseDate.hourOfDay().setCopy((Integer.valueOf(hour) + 12))
                        .minuteOfHour().setCopy(Integer.valueOf(minutesString)).toDate();
                if(new Date().after(dateWithMovieSchedule)) {
                    System.out.println("skip");
                    continue;
                }

                if(!allMovieHours && datesProcced.contains(new LocalDate(dateWithMovieSchedule))) {
                    continue;
                }

                datesProcced.add(new LocalDate(dateWithMovieSchedule));
                dates.add(dateWithMovieSchedule);
            } catch (IllegalStateException e) {
            }
        }
        return dates;
    }

    /**
     * @param theaterHeader: 'Sala : 07 3D 4DX', 'Sala : 05 3D esp.'
     * @return
     */
    private static String getRoomNumber(String theaterHeader) {
        Pattern p = Pattern.compile(":\\s([0-9A-Za-z]+)");
        System.out.println("theaterHeader: " + theaterHeader);
        Matcher m = p.matcher(theaterHeader);
        m.find();
        return m.group(1);
    }

    private static String getLanguage(String theaterHeader) {
        if (theaterHeader.contains("esp.")) {
            return SPANISH_LANGUAGE;
        }
        return ENGLISH_LANGUAGE;
    }

    private static String getSubLanguage(String theaterHeader) {
        if (theaterHeader.contains("esp.")) {
            return null;
        }
        return SPANISH_LANGUAGE;
    }
}
