package com.cinerd.scraper.api.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Edwin Ramirez Ventur on 8/10/2017.
 */
public interface TMDBMovieAPI {

    @GET("search/movie")
    Call<MovieSearchResponse> searchMoviesByName(@Query("api_key") String apiKey, @Query("query") String query);

    @GET("genre/movie/list")
    Call<GenreResult> getAllGenres(@Query("api_key") String apiKey);

    @GET("movie/{movieId}/videos")
    Call<MovieVideoResponse> getMovieVideos(@Path("movieId") long movieId, @Query("api_key") String apiKey);
}
