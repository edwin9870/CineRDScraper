package com.cinerd.scraper.api.rest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Edwin Ramirez Ventur on 8/10/2017.
 */
public class RottenTomatoesSearchResult {
    @SerializedName("actorCount")
    private int actorCount;
    @SerializedName("criticCount")
    private int criticCount;
    @SerializedName("franchiseCount")
    private int franchiseCount;
    @SerializedName("movieCount")
    private int movieCount;
    @SerializedName("movies")
    private List<Movies> movies;

    public int getActorCount() {
        return actorCount;
    }

    public void setActorCount(int actorCount) {
        this.actorCount = actorCount;
    }

    public int getCriticCount() {
        return criticCount;
    }

    public void setCriticCount(int criticCount) {
        this.criticCount = criticCount;
    }

    public int getFranchiseCount() {
        return franchiseCount;
    }

    public void setFranchiseCount(int franchiseCount) {
        this.franchiseCount = franchiseCount;
    }

    public int getMovieCount() {
        return movieCount;
    }

    public void setMovieCount(int movieCount) {
        this.movieCount = movieCount;
    }

    public List<Movies> getMovies() {
        return movies;
    }

    public void setMovies(List<Movies> movies) {
        this.movies = movies;
    }

    public static class CastItems {
        @SerializedName("name")
        private String name;
        @SerializedName("url")
        private String url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            return "CastItems{" +
                    "name='" + name + '\'' +
                    ", url='" + url + '\'' +
                    '}';
        }
    }

    public static class Movies {
        @SerializedName("castItems")
        private List<CastItems> castItems;
        @SerializedName("image")
        private String image;
        @SerializedName("meterClass")
        private String meterClass;
        @SerializedName("name")
        private String name;
        @SerializedName("subline")
        private String subline;
        @SerializedName("url")
        private String url;
        @SerializedName("year")
        private int year;

        public List<CastItems> getCastItems() {
            return castItems;
        }

        public void setCastItems(List<CastItems> castItems) {
            this.castItems = castItems;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getMeterClass() {
            return meterClass;
        }

        public void setMeterClass(String meterClass) {
            this.meterClass = meterClass;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSubline() {
            return subline;
        }

        public void setSubline(String subline) {
            this.subline = subline;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        @Override
        public String toString() {
            return "Movies{" +
                    "castItems=" + castItems +
                    ", image='" + image + '\'' +
                    ", meterClass='" + meterClass + '\'' +
                    ", name='" + name + '\'' +
                    ", subline='" + subline + '\'' +
                    ", url='" + url + '\'' +
                    ", year=" + year +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "RottenTomatoesSearchResult{" +
                "actorCount=" + actorCount +
                ", criticCount=" + criticCount +
                ", franchiseCount=" + franchiseCount +
                ", movieCount=" + movieCount +
                ", movies=" + movies +
                '}';
    }
}
