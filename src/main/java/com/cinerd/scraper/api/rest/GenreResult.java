package com.cinerd.scraper.api.rest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Edwin Ramirez Ventur on 8/10/2017.
 */
public class GenreResult {


    @SerializedName("genres")
    private List<Genre> genres;

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }
}
