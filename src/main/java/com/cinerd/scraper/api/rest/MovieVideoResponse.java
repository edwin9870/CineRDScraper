package com.cinerd.scraper.api.rest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Edwin Ramirez Ventur on 8/10/2017.
 */
public class MovieVideoResponse {


    @SerializedName("id")
    private int id;
    @SerializedName("results")
    private List<MovieVideoResult> results;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<MovieVideoResult> getResults() {
        return results;
    }

    public void setResults(List<MovieVideoResult> results) {
        this.results = results;
    }
}
