package com.cinerd.scraper;

import com.cinerd.scraper.api.rest.*;
import com.cinerd.scraper.entity.Movie;
import com.cinerd.scraper.entity.Movies;
import com.cinerd.scraper.entity.Rating;
import com.cinerd.scraper.entity.Theater;
import com.cinerd.scraper.util.ISODateAdapter;
import com.cinerd.scraper.util.JsoupUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import retrofit2.Call;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Edwin Ramirez Ventur on 8/10/2017.
 */
public class GetAllMovieList {

    public static final String TMDB_BACKDROP_BASE_URL = "https://image.tmdb.org/t/p/w342";
    public static final String TMDB_POSTER_BASE_URL = "https://image.tmdb.org/t/p/w185";
    public static final int RETRY_NUMBER = 3;
    public static final String IMDB_BASE_URL = "http://www.imdb.com";
    public static final String ROTTEN_TOMATOES_BASE_URL = "https://www.rottentomatoes.com";
    private static TMDBMovieAPI tmdbMovieAPI;

    /**
     *
     * @return Json with the movie details
     * @throws InterruptedException
     */
    public static String getMovieInformation(boolean allMovieHours) throws InterruptedException {
        List<Movie> movies = new ArrayList<>();
        tmdbMovieAPI = TMDBAppClient.getClient().create(TMDBMovieAPI.class);

        final String baseUrl = "http://cinemas.com.do";
        Document doc = JsoupUtil.sendSyncRequest(RETRY_NUMBER, Jsoup.connect(baseUrl + "/cartelera/cartelera-cines-dominicanos").userAgent("Mozilla").timeout(3000));
        Elements movieElementList = doc.select("#leftCol > div.user2 > div:nth-child(3) > div:nth-child(2) > ul li");
        for(Element movieElement : movieElementList) {

            try {
                Movie movie = new Movie();
                final String movieDetailUrl = baseUrl + movieElement.select("a").attr("href");
                System.out.println("MovieDetailUrl: " + movieDetailUrl);

                final Document movieDetailDocument = JsoupUtil.sendSyncRequest(RETRY_NUMBER, Jsoup.connect(movieDetailUrl).userAgent("Mozilla").timeout(3000));
                final List<Theater> theaters = GetMovieTheaterInformation.getTheaters(movieDetailDocument, allMovieHours);
                if(theaters == null || theaters.isEmpty()) {
                    continue;
                }
                movie.setTheaters(theaters);
                movie.setName(movieDetailDocument.select("#movie_info > span.movie_info_info_titulo").text());
                System.out.println("Movie name: " + movie.getName());
                final Short movieDuration = Short.valueOf(movieDetailDocument.select("#movie_info > span:nth-child(8)").text().replaceAll("\\D+", ""));
                movie.setDuration(movieDuration);
                movie.setSynopsis(movieDetailDocument.select("#movie_description_text > p:nth-child(1)").text());
                final MovieSearchResponse movieSearchResponse = tmdbMovieAPI.searchMoviesByName(TMDBAppClient.API, movie.getName()).execute().body();

                final MovieResults movieResult = movieSearchResponse.getResults().get(0);
                if(movieResult.getBackdropPath() != null) {
                    movie.setBackdropUrl(TMDB_BACKDROP_BASE_URL + movieResult.getBackdropPath());
                }

                if(movieResult.getPosterPath() != null) {
                    movie.setPosterUrl(TMDB_POSTER_BASE_URL + movieResult.getPosterPath());
                }
                movie.setReleaseDate(movieResult.getReleaseDate());
                movie.setGenre(getGenresName(movieResult.getGenreIds()));

                setMovieTrailer(movie, movieResult);
                setRating(movie);


                movies.add(movie);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }

        final Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new ISODateAdapter()).create();
        Movies moviesContainer = new Movies();
        moviesContainer.setmMovies(movies);
        return gson.toJson(moviesContainer);
    }

    private static void setMovieTrailer(Movie movie, MovieResults movieResult) throws IOException {
        final List<MovieVideoResult> movieVideoResults = tmdbMovieAPI.getMovieVideos(movieResult.getId(), TMDBAppClient.API).execute().body().getResults();
        if (movieVideoResults != null && movieVideoResults.size() > 0) {
            movie.setTrailerUrl("https://www.youtube.com/watch?v=" + movieVideoResults.get(0).getKey());
        }
    }

    private static void setRating(Movie movie) throws IOException, InterruptedException {
        //Rating
        final Rating rating = new Rating();
        setRottenTomatoesRating(movie, rating);
        setRatingImdb(movie, rating);
        movie.setRating(rating);
    }

    private static void setRatingImdb(Movie movie, Rating rating) throws InterruptedException, IOException {
        final String urlSearchImdb = IMDB_BASE_URL+"/find?q=" + URLEncoder.encode(movie.getName(), "UTF-8")+"&s=tt&exact=true&ref_=fn_al_tt_ex";
        final Document imdbSearchResult = JsoupUtil.sendSyncRequest(RETRY_NUMBER, Jsoup.connect(urlSearchImdb).userAgent("Mozilla").timeout(3000));
        final String hrefImdbResult = IMDB_BASE_URL +imdbSearchResult.select("#main > div > div.findSection > table > tbody > tr > td.result_text > a").attr("href");
        System.out.println("urlSearchImdb: " + urlSearchImdb);
        System.out.println("hrefImdbResult: " + hrefImdbResult);

        final Document ImdbMoviePage = JsoupUtil.sendSyncRequest(RETRY_NUMBER, Jsoup.connect(hrefImdbResult).userAgent("Mozilla").timeout(3000));
        final String ratingImdb = ImdbMoviePage.select("#title-overview-widget > div.vital > div.title_block > div > div.ratings_wrapper > div.imdbRating > div.ratingValue > strong > span").text();
        System.out.println("ratingImdb: " + ratingImdb);
        rating.setImdb(ratingImdb);
    }

    private static void setRottenTomatoesRating(Movie movie, Rating rating) throws IOException, InterruptedException {
        //Rottent
        final String urlSearchRottent = ROTTEN_TOMATOES_BASE_URL + "/search/?search=" + URLEncoder.encode(movie.getName(), "UTF-8");
        final Document rottentSearchResult = JsoupUtil.sendSyncRequest(RETRY_NUMBER, Jsoup.connect(urlSearchRottent).userAgent("Mozilla").timeout(3000));
        final String href = rottentSearchResult.select("#main_container script").html();
        Pattern p = Pattern.compile("eApiV2FrontendHost,(.+),\"tvCount\"");
        Matcher m = p.matcher(href);
        m.find();

        try {
            final String rottentJsonSearch = m.group(1).substring(m.group(1).indexOf("{")) + "}";
            final Gson gson = new Gson();
            final RottenTomatoesSearchResult rottenTomatoesSearchResult = gson.fromJson(rottentJsonSearch, RottenTomatoesSearchResult.class);
            System.out.println("urlSearchRottent: " + urlSearchRottent);
            System.out.println("rottenTomatoesSearchResult: " + rottenTomatoesSearchResult);

            final String movieUrlRottenTomatoes = ROTTEN_TOMATOES_BASE_URL + rottenTomatoesSearchResult.getMovies().get(0).getUrl();
            final String oficialScore = Jsoup.connect(movieUrlRottenTomatoes).userAgent("Mozilla").timeout(3000).get().select("#all-critics-numbers #tomato_meter_link > span.meter-value.superPageFontColor > span").text();
            System.out.println("oficialScore: " + oficialScore);
            final String audienceScore = Jsoup.connect(movieUrlRottenTomatoes).userAgent("Mozilla").timeout(3000).get().select("#scorePanel > div.col-sm-8.col-xs-12.audience-panel > div.audience-score.meter > a > div > div.media-body > div.meter-value > span").text();
            System.out.println("audienceScore: " + audienceScore);

            if (oficialScore.trim().length() > 0) {
                rating.setRottenTomatoes(oficialScore);
            } else if (audienceScore.trim().length() > 0) {
                rating.setRottenTomatoes(audienceScore.replaceAll("\\D+", ""));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<String> getGenresName(List<Integer> genresId) throws IOException {
        List<String> genresName = new ArrayList<>();

        final Call<GenreResult> allGenresCall = tmdbMovieAPI.getAllGenres(TMDBAppClient.API);
        final GenreResult genreResults = allGenresCall.execute().body();
        for(Genre genre : genreResults.getGenres()) {
            if(genresId.contains(genre.getId())) {
                genresName.add(genre.getName());
            }
        }


        return genresName;
    }
}
