package com.cinerd.scraper.util;

import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import retrofit2.Call;

/**
 * Created by Edwin Ramirez Ventur on 8/10/2017.
 */
public class JsoupUtil {

    public static final int WAIT_TIME = 800;

    public static Document sendSyncRequest(int retryNumber, Connection connection) throws InterruptedException {
        int i = 0;

        while(i < retryNumber) {
            try {
                return connection.get();
            } catch (Exception e) {
                i++;
                e.printStackTrace();
            } finally {
                Thread.sleep(WAIT_TIME);
            }
        }
        return null;
    }
}
