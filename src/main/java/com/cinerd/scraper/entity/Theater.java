
package com.cinerd.scraper.entity;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.io.Serializable;
import java.util.List;

@Generated("net.hexar.json2pojo")
public class Theater implements Serializable {

    @SerializedName("name")
    private String mName;
    @SerializedName("room")
    private List<Room> mRoom;

    public Theater() {}

    public Theater(String mName) {
        this.mName = mName;
    }


    public String getmName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public List<Room> getmRoom() {
        return mRoom;
    }

    public void setRoom(List<Room> mRoom) {
        this.mRoom = mRoom;
    }

    @Override
    public String toString() {
        return "Theater{" +
                "mName='" + mName + '\'' +
                ", mRoom=" + mRoom +
                '}';
    }
}
