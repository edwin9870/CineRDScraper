
package com.cinerd.scraper.entity;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("net.hexar.json2pojo")
public class Movies {

    @SerializedName("movies")
    private List<Movie> mMovies;


    public List<Movie> getmMovies() {
        return mMovies;
    }

    public void setmMovies(List<Movie> mMovies) {
        this.mMovies = mMovies;
    }

    @Override
    public String toString() {
        return "Movies{" +
                "mMovies=" + mMovies +
                '}';
    }

}
