package com.cinerd.scraper.entity;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.Date;
import java.util.List;

/**
 * Created by Edwin Ramirez Ventur on 8/10/2017.
 */
@Generated("net.hexar.json2pojo")
public class Movie {

    @SerializedName("duration")
    private Short duration;
    @SerializedName("genre")
    private List<String> genre;
    @SerializedName("name")
    private String name;
    @SerializedName("rating")
    private Rating rating;
    @SerializedName("release_date")
    private Date releaseDate;
    @SerializedName("synopsis")
    private String synopsis;
    @SerializedName("poster_url")
    private String posterUrl;
    @SerializedName("backdrop_url")
    private String backdropUrl;
    @SerializedName("trailer_url")
    private String trailerUrl;
    @SerializedName("theaters")
    private List<Theater> theaters;

    public Short getDuration() {
        return duration;
    }

    public void setDuration(Short duration) {
        this.duration = duration;
    }

    public List<String> getGenre() {
        return genre;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public void setName(String mName) {
        this.name = mName;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getBackdropUrl() {
        return backdropUrl;
    }

    public void setBackdropUrl(String backdropUrl) {
        this.backdropUrl = backdropUrl;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public List<Theater> getTheaters() {
        return theaters;
    }

    public void setTheaters(List<Theater> theaters) {
        this.theaters = theaters;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "duration=" + duration +
                ", genre=" + genre +
                ", name='" + name + '\'' +
                ", rating=" + rating +
                ", releaseDate=" + releaseDate +
                ", synopsis='" + synopsis + '\'' +
                ", posterUrl='" + posterUrl + '\'' +
                ", backdropUrl='" + backdropUrl + '\'' +
                ", trailerUrl='" + trailerUrl + '\'' +
                ", theaters=" + theaters +
                '}';
    }
}
