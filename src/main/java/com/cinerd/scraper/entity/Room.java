
package com.cinerd.scraper.entity;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.io.Serializable;
import java.util.Date;

@Generated("net.hexar.json2pojo")
public class Room implements Serializable {

    @SerializedName("date")
    private Date mDate;
    @SerializedName("format")
    private String mFormat;
    @SerializedName("language")
    private String mLanguage;
    @SerializedName("name")
    private String name;
    @SerializedName("subtitle")
    private String mSubtitle;


    public Date getDate() {
        return mDate;
    }

    public String getFormat() {
        return mFormat;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public String getNumber() {
        return name;
    }

    public String getSubtitle() {
        return mSubtitle;
    }

    public void setmDate(Date mDate) {
        this.mDate = mDate;
    }

    public void setmFormat(String mFormat) {
        this.mFormat = mFormat;
    }

    public void setmLanguage(String mLanguage) {
        this.mLanguage = mLanguage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setmSubtitle(String mSubtitle) {
        this.mSubtitle = mSubtitle;
    }

    @Override
    public String toString() {
        return "Room{" +
                "mDate='" + mDate + '\'' +
                ", mFormat='" + mFormat + '\'' +
                ", mLanguage='" + mLanguage + '\'' +
                ", name='" + name + '\'' +
                ", mSubtitle='" + mSubtitle + '\'' +
                '}';
    }
}
