
package com.cinerd.scraper.entity;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
public class Rating {

    @SerializedName("imdb")
    private String imdb;
    @SerializedName("rottenTomatoes")
    private String rottenTomatoes;

    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }

    public String getRottenTomatoes() {
        return rottenTomatoes;
    }

    public void setRottenTomatoes(String rottenTomatoes) {
        this.rottenTomatoes = rottenTomatoes;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "imdb='" + imdb + '\'' +
                ", rottenTomatoes='" + rottenTomatoes + '\'' +
                '}';
    }

}
